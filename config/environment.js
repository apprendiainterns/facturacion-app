'use strict';

module.exports = function(environment) {
  let ENV = {
    modulePrefix: 'facturacion-app',
    environment,
    rootURL: '',
    locationType: 'hash',
    torii: {
      sessionServiceName: 'session'
    },
    EmberENV: {
      FEATURES: {
        // Here you can enable experimental features on an ember canary build
        // e.g. 'with-controller': true
      },
      EXTEND_PROTOTYPES: {
        // Prevent Ember Data from overriding Date.parse.
        Date: false
      }
    },

    APP: {
      // Here you can pass flags/options to your application instance
      // when it is created
    }
  };

  if (environment === 'development') {
    // ENV.APP.LOG_RESOLVER = true;
    // ENV.APP.LOG_ACTIVE_GENERATION = true;
    // ENV.APP.LOG_TRANSITIONS = true;
    // ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
    // ENV.APP.LOG_VIEW_LOOKUPS = true;
    ENV['firebase'] = {
      // TUDU: add configuration to development db 
      
      apiKey: "AIzaSyDAQiwxQg9DOmwdDtRYtk_xx-cRd1Gscbw",
      authDomain: "reto3facturacion.firebaseapp.com",
      databaseURL: "https://reto3facturacion.firebaseio.com",
      projectId: "reto3facturacion",
      storageBucket: "reto3facturacion.appspot.com",
      messagingSenderId: "161046448873"
    }
  }

  if (environment === 'test') {
    // Testem prefers this...
    ENV.locationType = 'none';

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.APP.rootElement = '#ember-testing';
    ENV.APP.autoboot = false;
  }

  if (environment === 'production') {
    // here you can enable a production-specific feature
    ENV['firebase'] = {
      // TUDU: add configuration to production db 
    }
  }

  return ENV;
};
