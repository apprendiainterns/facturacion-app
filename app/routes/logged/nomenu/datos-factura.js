import Route from '@ember/routing/route';

export default Route.extend({
    setupController(controller, model){
        this._super(...arguments);
        let momentDate = moment(this.get('model.fecha'))
        let dayFactura=[]

        moment.locale('es')

        const today = moment()
        let limit = today.clone().subtract(72, 'hours').startOf('day')

        while(limit.isBefore(today)){
            dayFactura.push(today.clone())
            today.subtract(24, 'hours').startOf('day')
        }

        controller.setProperties({
            day: `${momentDate.date()}`,
            month: `${momentDate.month() + 1}`,
            year: `${momentDate.year()}`,
            facturar: dayFactura
        })
    }
});
