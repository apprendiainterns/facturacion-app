import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import moment from 'moment';

export default Route.extend({
    menu: service('lowermenu'),
    model(params) {
        return this.store.find('factura', params.f_id)
    },
    afterModel(model) {
        if (model) {
            model.set('fecha', moment().toDate())
        }
    },
    resetController(controller, isExiting, transition) {
/* 
    Cancela la factura si la transition es diferente a los espeficado y hace visible el menu
    inferior
 */
        if (isExiting) {
            if ((transition.targetName !== 'logged.menu.catalogoconceptos') &&
                (transition.targetName !== 'logged.menu.conceptoseimpuestos')) {
                this.get('menu').visible()
                this.get('currentModel').rollbackAttributes();
            }
        }
    }
    
});