import Route from '@ember/routing/route';

export default Route.extend({
    queryParams: {
        source: {
            refreshModel: true
        }
    },
    model() {
        return this.store.createRecord('catalogoConcepto', {
            validateCantidad: false,
            validateImporte: false,
            isCatalogo: true
        })
    },

    actions: {
        willTransition( /*transition*/) {
            let concepto = this.get('currentModel');
            if (concepto.get('hasDirtyAttributes')) {
                concepto.rollbackAttributes();
                concepto.get('impuestos').invoke('rollbackAttributes');
                concepto.get('partes').invoke('rollbackAttributes');
            }
        }
    }
});
