import Route from '@ember/routing/route';

export default Route.extend({

	model() {
		return this.store.createRecord('fiscalReceptor', {
			manuallyCreated: true
		});
	},

	afterModel(model/*, transition*/) {
		if (model) {
			model.get('direcciones').createRecord({});
		}
	},

	actions: {
		willTransition( /*transition*/ ) {
			let currentModel = this.get('currentModel');

			let direcciones = currentModel.hasMany('fiscalDirecciones').value();
			if (direcciones) {
				direcciones.invoke("rollbackAttributes");
			}

			currentModel.rollbackAttributes();
		}
	},
});
