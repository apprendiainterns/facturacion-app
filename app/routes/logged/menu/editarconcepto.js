import Route from '@ember/routing/route';

export default Route.extend({
    model(params) {
		return this.store.findRecord('catalogo-concepto', params.concepto_id).catch(()=>{
			return null;
		});
	},

	afterModel(model) {
		if(!model){
			return this.transitionTo('logged.menu.catalogoconceptos');
		}
	},

	actions: {

		willTransition(/*transition*/) {
			let currentModel = this.get('currentModel');

			if(currentModel.get('isDeleted')){
				currentModel.save();
			} else {
				currentModel.rollbackAttributes();
			}
		}

	},
});
