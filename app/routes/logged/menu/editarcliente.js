import Route from '@ember/routing/route';

export default Route.extend({
	model(params) {
		let id = params.receptor_id;
		return this.store.findRecord("fiscalReceptor", id)
	},

	afterModel(model) {
		model.get('direcciones').then((arr)=>{
			if(arr.length == 0){
				model.get('direcciones').createRecord({});
			}
		}).catch(()=>{
			if(model.get('direcciones.length') == 0){
				model.get('direcciones').createRecord({});
			}
		})
    },

	actions: {

		willTransition( /*transition*/ ) {
			if (this.get('currentModel.hasDirtyAttributes')) {
				this.get('currentModel').rollbackAttributes();
			}
			
		}

	}
});
