import Application from '@ember/application';
import Resolver from './resolver';
import loadInitializers from 'ember-load-initializers';
import config from './config/environment';
import Inflector from 'ember-inflector';

const App = Application.extend({
  modulePrefix: config.modulePrefix,
  podModulePrefix: config.podModulePrefix,
  Resolver
});

loadInitializers(App, config.modulePrefix);

Inflector.inflector.irregular('emisor', 'emisores');
Inflector.inflector.irregular('receptor', 'receptores');

export default App;
