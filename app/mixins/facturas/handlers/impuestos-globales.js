import { computed } from '@ember/object';
import { alias } from '@ember/object/computed';
import Mixin from '@ember/object/mixin';
import Decimal from 'npm:decimal.js-light';
import { inject as service } from '@ember/service';


export default Mixin.create({
	precision: service('round-precision'),

	impuestosGlobales: alias('model.impuestosGlobales.[]'),

	actions: {
		addImpuestoGlobal() {
			let factura = this.get('model');
			let impuesto = factura.get('impuestosGlobales').createRecord({
				isGlobal: true,
				facturaId: factura.get('id')
			});

			impuesto.reopen({
				base: computed('isLocal', 'precision.decimals', function () {
					if (this.get('isLocal')) {
						return Decimal(factura.get('preTotal')).todp(this.get('precision.decimals')).toNumber(); //this.get('factura.preTotal');
					} else {
						return Decimal(factura.get('subtotal')).todp(this.get('precision.decimals')).minus(factura.get('descuento')).todp(this.get('precision.decimals')).toNumber(); //this.get('factura.subtotal');
					}
				}).meta({ serialize: true })
			});
		},


		removedImpuestoGlobal( impuesto ) {
			debugger
			let factura = this.get('factura');
			factura.get('impuestosGlobales').removeObject(impuesto);
		}

	}
});
