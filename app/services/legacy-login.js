import Service from '@ember/service';
import { inject as service } from '@ember/service';
import { get } from '@ember/object';
import fetch from 'fetch';
import { all } from 'rsvp';
import moment from 'moment';
import Promise from 'rsvp';

export default Service.extend({
	session: service(),
	store: service(),
	firebaseApp: service(),
	currentUser: service(),


	sleep(s) {
		return new Promise(resolve => setTimeout(resolve, s * 1000));
	},


	createAccount(info) {
		return this.get('firebaseApp').auth()
			.createUserWithEmailAndPassword(
				get(info, 'email'),
				get(info, 'password')
			)
	},


	migrateData(oldData, options) {
		options = options || {autosave: true}
		let store = this.get('store');
		let promises = [];
		let newEmisores, newReceptores;

		let emisores = get(oldData, 'emisores')
		if (emisores) {
			newEmisores = emisores.map((attr) => {
				let emisor = store.createRecord('fiscalEmisor', {
					nombre: get(attr, 'nombre'),
					rfc: get(attr, 'rfc'),
					email: get(attr, 'email')
				});

				get(attr, 'direcciones').forEach((attr) => {
					emisor.get('direcciones').createRecord({
						direccion: `${get(attr, 'calle')} ${get(attr, 'no_exterior')} ${get(attr, 'no_interior')} ${get(attr, 'colonia')} ${get(attr, 'municipio')}`,
						cp: get(attr, 'cp')
					});
				});

				get(attr, 'certificados').forEach((attr) => {
					emisor.get('certificados').createRecord({
						rfc: get(attr, 'rfc'),
						numero: get(attr, 'numero'),
						pemCer: get(attr, 'pem_cer'),
						pemKey: get(attr, 'pem_key'),
						pemKeySecure: get(attr, 'pem_key_secure'),
						notAfter: moment(get(attr, 'not_after')).utc().toDate(),
						notBefore: moment(get(attr, 'not_before')).utc().toDate(),
						testOnly: get(attr, 'test_only')
					});
				});

				emisor.get('templates').createRecord({
					code: 'basic',
					label: 'General'
				});



				return emisor;
			});

			if(options.autosave){
				promises.push(newEmisores.invoke('fullSave'));
			}
		}


		let receptores = get(oldData, 'receptores')
		if (receptores) {
			newReceptores = receptores.map((attr) => {
				let receptor = store.createRecord('fiscalReceptor', {
					nombre: get(attr, 'nombre'),
					rfc: get(attr, 'rfc'),
					email: get(attr, 'email')
				});

				get(attr, 'direcciones').forEach((attr) => {
					receptor.get('direcciones').createRecord({
						direccion: `${get(attr, 'calle')} ${get(attr, 'no_exterior')} ${get(attr, 'no_interior')} ${get(attr, 'colonia')} ${get(attr, 'municipio')}`,
						cp: get(attr, 'cp')
					});
				});

				return receptor;
			});

			if(options.autosave){
				promises.push(newReceptores.invoke('fullSave'));
			}
		}

		if(options.autosave){
			return all(promises);
		}else{
			return Promise.resolve({
				emisores: newEmisores,
				receptores: newReceptores
			});
		}
	},


	login(params, options) {
		options = options || { signup: true, automigrate: true };

		return fetch('https://facturacion32.contamc.com/migration', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(params)
		}).then((response) => {
			return response.json().then((oldData) => {
				let promise;

				if (options.signup) {
					promise = this.createAccount(params).then(() => {
						params.provider = 'password';

						return this.get('session').open('firebase', params).then(() => {
							return this.get('currentUser.model');
						});
					});
				} else {
					promise = Promise.resolve();
				}

				return promise.then(() => {
					if(options.automigrate){
						return this.migrateData(oldData);
					}else{
						return oldData;
					}
				});
			});
		});
	}
});
