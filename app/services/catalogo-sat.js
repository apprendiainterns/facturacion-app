import Elastic from './elasticsearch';

export default Elastic.extend({
	/**
	 * @override: ember lifecycle
	 */
	init(...params) {
		this._super(...params);
		this.set('defaultOptions',{
			index: 'sat3_3',
			_source: true,
		});
	}
});
