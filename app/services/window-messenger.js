import Service from '@ember/service';
import {set} from '@ember/object';

export default Service.extend({
	eventPrefix: 'contamc',
	readySent: false,

	init() {
		this._super.apply(...arguments);

		this.set('_callbacksQueue', []);
		this.set('_unlistenedMessages', []);

		if (!window.opener) { return }
		window.addEventListener('message', (event) => {
			// TODO: Verify origin domain
			// let domain = evento.origin;
			if(!this._validEventType(event)){ return }

			let data = event.data || {},
					type = this._eventType(event)
					/*,source = event.source*/;
			// console.log(this._eventType(event), type)

			let callbacksChain = this._callbacksForType(type);
			if(callbacksChain.length > 0){
				callbacksChain.forEach((obj)=>{ obj.callback(data.payload) });
			} else{
				this.get('_unlistenedMessages').push({type: type, payload: data.payload});
			}
		});
		this._notifyReadiness();
	},




	emit(type, payload) {
		if(window.opener){
			window.opener.postMessage({ type: `${this.get('eventPrefix')}/${type}`, payload: payload }, '*');
		}
	},




	on(type, callback) {
		this.get('_callbacksQueue').push({ type: type, callback: callback });
		this._notifyReadiness();
		this._syncUnlistened(type);
	},



	off(type, callback){
		if(callback){
			let obj = this._callbacksForType(type).findBy('callback', callback);
			this.get('_callbacksQueue').removeObject(obj);
		} else{
			this.get('_callbacksQueue').removeObjects(this._callbacksForType(type));
		}
	},


	_syncUnlistened(type){
		let _unlistenedMessages = this.get('_unlistenedMessages');

		_unlistenedMessages.filterBy('type', type).forEach((unlistened)=>{
			this._callbacksForType(type).forEach((obj)=>{
				obj.callback(unlistened.payload);
			});
			set(unlistened, 'pendingToRemove', true);
		});

		_unlistenedMessages.removeObjects(_unlistenedMessages.filterBy('pendingToRemove'));
	},



	_callbacksForType(type){
		return this.get('_callbacksQueue').filterBy('type', type);
	},



	_notifyReadiness() {
		if (!this.get('readySent')) {
			window.addEventListener('unload', () => { this.emit('unload') });
			this.emit('ready');
			this.set('readySent', true);
		}
	},



	_eventType(event) {
		let data = event.data || {};
		return `${data.type}`.replace(`${this.get('eventPrefix')}/`, '');
	},

	_validEventType(event){
		let data = event.data || {};
		return `${data.type}`.indexOf(`${this.get('eventPrefix')}/`) != -1;
	}
});
