import Component from '@ember/component';
import { inject as service} from '@ember/service';

export default Component.extend({
    menu: service('lowermenu'),
    keyboard: service('ember-cordova/keyboard'),
    keyboardIsShowing: false,
    /* 
        Ocultar y desocultar teclado
    */
    didInsertElement() {
        this._super();

        this.get('keyboard').on('keyboardDidShow', this.onkeyboardDidShow.bind(this));
        this.get('keyboard').on('keyboardDidHide', this.onkeyboardDidHide.bind(this));
    },

    willDestroyElement() {
        this._super();

        this.get('keyboard').off('keyboardDidShow', this.onkeyboardDidShow.bind(this));
        this.get('keyboard').off('keyboardDidHide', this.onkeyboardDidHide.bind(this));
    },

    onkeyboardDidShow() {
        if (this.isDestroyed || this.isDestroying) {
          return null              
        } else {
            this.set('keyboardIsShowing', true);            
        }
    },

    onkeyboardDidHide() {
        if (this.isDestroyed || this.isDestroying) {
          return null  
        } else {
            this.set('keyboardIsShowing', false);
        }        
    },

   actions:{
/* --------------------Emisor--------------------------- */
    nextemisor() {        
        this.get('nextEmisor')()
    },
/* -------------------Datos Factura---------------------------- */
    backdatos() {
        this.get('backDatos')()
    },
    nextdatos() {
        this.get('nextDatos')()
    },
/* ---------------------Conceptos-------------------------- */
    backconceptos() {
        this.get('backConceptos')()
    },
    nextconceptos() {
        this.get('nextConceptos')()
    },
/* ---------------------Impuestos-------------------------- */
    backimpuestos() {
        this.get('backImpuestos')()        
    },
    nextimpuestos() {
        this.get('nextImpuestos')()
    },
/* ---------------------Complementos-------------------------- */
    backc_ine() {
        this.get('backC_ine')()
    },
    nextc_ine() {
        this.get('nextC_ine')()
    },
/* ------------------------Resumen----------------------- */
    backresumen() {
        this.get('backResumen')()        
    },
    /* 
    TO DO
    
    Guardar la factura
    */
    facturar(){
        this.get('save')()
    }

   }
});