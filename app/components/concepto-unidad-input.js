import { get } from '@ember/object';
import Component from '@ember/component';

export default Component.extend({
  unidadQuery(term) {
    return {
      query: {
        query_string: {
          fields: ['clave^5', 'name'],
          query: `${term}*`
        }
      },
      sort: [
        {"name.keyword": "asc"}
      ],
      size: 10000
    }
  },

  actions: {
    setClaveUnidad(obj) {
      if (obj) {
        this.set('concepto.claveUnidad', get(obj, 'clave'));
        // this.set('concepto.unidad', Ember.get(obj, 'name'));
        this.set('concepto.claveUnidadSelected', obj);
      } else {
        this.set('concepto.claveUnidad', null);
        this.set('concepto.claveUnidadSelected', null);
        // this.set('concepto.unidad', null);
      }
    }
  }
});
