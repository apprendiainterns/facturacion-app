import Component from '@ember/component';
import { makeArray } from '@ember/array';
import INEConstants from 'ember-cli-facturacion-logic/utils/catalogos/complemento-ine';
import { computed, observer } from '@ember/object';

export default Component.extend({
  ine: null,
  constants: INEConstants,


  showEntidadesFields: computed('ine.{tipoProceso,tipoComite}', function() {
    let { tipoProceso, tipoComite } = this.get('ine').getProperties('tipoProceso', 'tipoComite');

    return (tipoProceso == INEConstants.ORDINARIO && tipoComite && tipoComite != INEConstants.EJENACIONAL) || (tipoProceso == INEConstants.PRECAMPANA) || (tipoProceso == INEConstants.CAMPANA);
  }),

  disableTipoComite: computed('ine.{tipoProceso}', function() {
    return this.get('ine.tipoProceso') != INEConstants.ORDINARIO ? true : null;
  }),

  disableIdContabilidad: computed('ine.{tipoProceso,tipoComite}', function() {
    return this.get('ine.tipoComite') == INEConstants.EJEESTATAL || this.get('ine.tipoProceso') != INEConstants.ORDINARIO ? true : null;
  }),

  disableEntidadAmbito: computed('ine.{tipoProceso}', function() {
    return this.get('ine.tipoProceso') == INEConstants.ORDINARIO ? true : null
  }),


  flagsObserver: observer('showEntidadesFields', 'disableTipoComite', 'disableIdContabilidad', 'disableEntidadAmbito', function() {
    if (this.get('ine.isFulfilled')) {
      if (this.get('disableTipoComite')) {
        this.set('ine.tipoComite', null);
      }

      if (this.get('disableIdContabilidad')) {
        this.set('ine.idContabilidad', null);
      }

      if (this.get('ine.entidades.isFulfilled')) {
        if (this.get('showEntidadesFields')) {
          if (this.get('ine.entidades.length') == 0) {
            this.get('ine.entidades').createRecord();
          }
        } else {
          this.get('ine.entidades').invoke('deleteRecord');
        }

        if (this.get('disableEntidadAmbito')) {
          this.get('ine.entidades').forEach(function(entidad) {
            return entidad.set('ambito', null);
          })
        }
      }
    }
  }),
/* 
  Acciones por si la factura cuanta complementos ine
 */

  actions: {
    addIneEntidad() {
      return this.get('ine.entidades').createRecord();
    },

    removeIneEntidad(ineEntidad) {
      return ineEntidad.deleteRecord();
    },

    addIneContabilidad(ineEntidad, idContabilidad) {
      return ineEntidad.get('contabilidades').createRecord({ idContabilidad: idContabilidad })
    },

    removeIneContabilidad(ineEntidad, index) {
      return ineEntidad.get('contabilidades').objectAt(index).deleteRecord();
    }
  }

});
