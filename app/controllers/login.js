import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import { validator, buildValidations } from "ember-cp-validations";

const Validations = buildValidations({
	'usuario': [
		validator('presence', {
			presence: true,
			message: 'El campo usuario no puede estar vacio'
		}),
		validator('format', {
			type: 'email',
			message: 'Esto no es un correo'
		})
	],
	'pass':
		validator('presence', {
			presence: true,
			message: 'El campo contraseña no puede estar vacio'
		})

})

export default Controller.extend(Validations, {
	session: service(),

	actions: {
		toogleError(attr) {
			switch (attr) {
				case 'usuario':
					this.set('msgemail', '')
					this.set('userErrorCheck', true)
					break
				case 'pass':
					this.set('msgpass', '')
					this.set('passErrorCheck', true)
					break
			}
		},

		login(user, pass) {
			this.validate().then(({ validations }) => {
				if (validations.get('isValid')) {

					this.get('session').open('firebase', {
						provider: 'password',
						email: user,
						password: pass

					}).then(() => {

						this.set("usuario", '')
						this.set("pass", '')
						this.set('userErrorCheck', false)
						this.set('passErrorCheck', false)
						this.set('msgemail', '')
						this.set('msgpass', '')

						this.transitionToRoute("logged.menu.factura")
					}).catch((e) => {

						switch (e.code) {
							case 'auth/user-not-found':
								this.set('msgemail', 'El usuario no existe')
								break;
							case 'auth/wrong-password':
								this.set('msgpass', 'Contraseña incorrecta')
								break;
							default:
								break;
						}
					})
				}
			})

		}

	}
});