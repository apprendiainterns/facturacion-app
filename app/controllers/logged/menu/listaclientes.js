import Controller from '@ember/controller';
import { inject as service} from '@ember/service';
import { computed } from '@ember/object'

export default Controller.extend({
    
    menu: service('lowermenu'),
        
    searchResults: computed('model.[]', 'model.@each.rfc', 'model.@each.nombre', 'busqueda', function(){
        if(!this.get('busqueda') || this.get('busqueda') == ''){
            return this.get('model')
        }
    
        return this.get('model').filter((cliente)=>{
            return (cliente.get('rfc').indexOf(this.get('busqueda').toUpperCase()) != -1 || 
    cliente.get('nombre').toLowerCase().indexOf(this.get('busqueda').toLowerCase()) != -1)
        })
    }),

    actions: {
        crearFactura() {
            this.get('menu').invisible()

            let factura = this.store.createRecord('factura')
            this.transitionToRoute('logged.nomenu', factura.id )
        }
        
    }
});
