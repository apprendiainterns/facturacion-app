import Controller from '@ember/controller';

export default Controller.extend({
    queryParams:['source','id'],
    source: 'logged.menu.catalogoconceptos',
    id: null
});
