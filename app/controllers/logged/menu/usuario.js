import Controller from '@ember/controller';
import { validator, buildValidations } from "ember-cp-validations";
import { inject as service } from '@ember/service';

const Validations = buildValidations({

    'password':[
        
        validator('presence', {
            presence: true,
            message: 'El campo nueva contraseña es obligatorio'
        }),
        validator('length', {
            min: 6,
            message: 'La contraseña debe ser minimo de 6 caracteres'
          })
    ],
    'verifyPassword':
        validator('confirmation', {
            on: 'password',
            message: 'la contraseña no coincide'
        })
})

export default Controller.extend(Validations, {
    firebase: service(),
    actions: {
        toogleError(attr) {

            switch (attr) {
                case 'password':
                    this.set('passwordErrorCheck', true)
                    break
                case 'verifyPassword':
                    this.set('verifyPasswordErrorCheck', true)
                    break
            }
        },

        changepassword() {
            this.validate().then(({ validations }) => {
                if (validations.get('isValid')) {

                    let changep = this;
                    swal({
                        title: 'Ingresa la contraseña actual',
                        input: 'password',
                        showCancelButton: true,
                        confirmButtonText: 'Aceptar',
                        cancelButtonText: 'Cancelar',
                        showLoaderOnConfirm: true,
                        preConfirm: (pass) => {

                            if (!changep.email) {
                                changep.set('email', changep.get('session.currentUser.email'))
                                changep.set('uid', changep.get('session.currentUser.uid'))
                            }

                            return new Promise((resolve, reject) => {

                                return changep.get('session').close().then(() => {
                                    return changep.get('session').open('firebase', {
                                        provider: 'password',
                                        email: changep.email,
                                        password: pass
                                    }).then(() => {
                                        return resolve()
                                    }).catch(() => {
                                        window.swal.showValidationError(
                                            'Verifique su contraseña ya que es incorrecta'
                                        )
                                        return resolve()
                                    })

                                }).catch(() => {
                                    return changep.get('session').open('firebase', {
                                        provider: 'password',
                                        email: changep.email,
                                        password: pass
                                    }).then(() => {
                                        return resolve()
                                    }).catch(() => {
                                        window.swal.showValidationError(
                                            'Verifique su contraseña ya que es incorrecta'
                                        )
                                        return resolve()
                                    })
                                })
                            })

                        },
                        allowOutsideClick: () => !swal.isLoading()
                    }).then(() => {
                        var user = this.get('session.currentUser');

                        user.updatePassword(this.password).then(() => {

                            swal({
                                title: `La contraseña para ${this.email}`,
                                type: 'success',
                                text: 'Fue cambiada con exito'
                            })

                        }).catch((error) => {

                        });
                        this.set("password", '')
                        this.set("verifyPassword", '')
                        this.set('passwordErrorCheck', false)
                        this.set('verifyPasswordErrorCheck', false)
                    })
                } else {                                       
                    swal({
                        title: 'Verificar los campos',
                        type: 'error',
                        text: 'que estén correctos'
                    })
                }

            })
        },

        logout() {
            swal({
                title: '¿Esta seguro de cerrar sesión?',
                text: 'Esta a punto de abandonar la APP',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then((result) => {

                if (result.value) {
                    this.get('session').close().then(() => {
                        this.transitionToRoute('login')
                    })
                }
            });
        },

    }

});

