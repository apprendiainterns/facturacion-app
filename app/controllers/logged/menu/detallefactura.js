import Controller from '@ember/controller';

export default Controller.extend({
    actions:{
        pressed: function(trgt){
            event
             //prevent the default action for the click event
            event.preventDefault();

            //get the top offset of the target anchor
            var target_offset = window.$("#"+trgt).offset();
            var target_top = target_offset.top;

            //goto that anchor by setting the body scroll top to anchor top
            window.$('html, body').animate({scrollTop:target_top}, 500);
        }
    }
});
