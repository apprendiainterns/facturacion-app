import Controller from '@ember/controller';
import { validator, buildValidations } from "ember-cp-validations";
import fetch from 'fetch';
import { computed } from '@ember/object'
import { all } from 'rsvp';

const Validations = buildValidations({
    'model.rfc': [
        validator('presence', {
            presence: true,
            message: 'El campo RFC debe estar lleno'
        }),
        validator('format', {
            regex: /^([A-ZÑ&]{3,4}) ?(?:- ?)?(\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])) ?(?:- ?)?([A-Z\d]{2})([A\d])$/,
            message: 'El RFC no tiene el formato correcto'

        })
    ],
    'model.nombre':
        validator('presence', {
            presence: true,
            message: 'El campo Nombre o razón social no puede estar vacio'
        }),
    'model.email': [
        validator('presence', {
            presence: true,
            message: 'El campo email no puede estar vacio'
        }),
        validator('format', {
            type: 'email',
            message: 'Esto no es un correo'
        })
    ],
    'cp': [
        validator('presence', {
            presence: true,
            message: 'El campo codigo postal debe estar lleno'
        }),
        validator('format', {
            regex: /^(?:0[1-9]\d{3}|[1-4]\d{4}|5[0-2]\d{3})$/,
            message: 'El codigo postal no tiene el formato correcto'

        })
    ],
})

export default Controller.extend(Validations, {
    codigosPostales: null,
    newDireccion: computed('model.direcciones.[]', function () {
        return this.get('model.direcciones.lastObject');
    }),

    actions: {
        toogleError(attr) {
            switch (attr) {
                case 'rfc':
                    this.set('rfcErrorCheck', true)
                    break
                case 'nomRazon':
                    this.set('nomRazonErrorCheck', true)
                    break
                case 'email':
                    this.set('emailErrorCheck', true)
                    break
                case 'cp':
                    this.set('cpErrorCheck', true)
                    break
            }
        },

        createReceptor() {
            this.validate().then(({ validations }) => {
                if (validations.get('isValid')) {

                    let receptor = this.get('model'),
                        newDireccion = this.get('newDireccion')
                        
                    this.set('isSaving', true)
                    
                    this.get('newDireccion').set('direccion', `${this.calle} ${this.numExt} ${this.numInt} ${this.colonia}`)
                    this.get('newDireccion').set('cp', this.cp)

                    return all([
                        newDireccion.save(),
                        receptor.get('ine').then((ine)=> {if(ine) return ine.fullSave()})
                    ]).then(() => {
                        return receptor.save().then(() => {
                            this.set('isSaving', false);
                            
                            this.set('model.rfc', '')
                            this.set('model.nomRazon', '')
                            this.set('model.email', '')
                            this.set('cp', '')
                            this.set('estado', '')
                            this.set('municipio', '')
                            this.set('col', '')
                            this.set('calle', '')
                            this.set('numExt', '')
                            this.set('numInt', '')
                            this.set('rfcErrorCheck', false)
                            this.set('nomRazonErrorCheck', false)
                            this.set('emailErrorCheck', false)
                            this.set('cpErrorCheck', false)

                            this.transitionToRoute("logged.menu.listaclientes")
                        });
                    });

                }
            })
        },

        /* 
            Accion para obtener el estado, municipio y las posibles colonias
        */
        codigoPostal() {
            let codigo = this.get('cp')
            
            return fetch(`http://192.168.100.45:8989/api/cp/${codigo}`).then((response) => {
                response.json().then((datos) => {
                    this.set('codigosPostales', datos.data)
                    this.set('estado', this.codigosPostales.firstObject.estado)
                    this.set('municipio', this.codigosPostales.firstObject.municipio)
                })
            });
        },
        toggleDatosIne(checked) {
            if (checked) {
                this.set('model.ine', this.get('model.store').createRecord('complemento/ine'))
            } else if (this.get('model.ine.content') && (this.get('model.ine.isNew') || confirm('Va a borrar los datos INE de este cliente ¿desea continuar?'))) {
                return this.get('model.ine').then((ine) => ine.destroyRecord()).then(() => this.get('model').save());
            }
        }
    }
});
