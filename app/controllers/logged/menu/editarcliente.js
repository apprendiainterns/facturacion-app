import { all } from 'rsvp';
import Controller from '@ember/controller';
import {computed} from '@ember/object';

export default Controller.extend({
    codigosPostales: null,
    
    newDireccion: computed('model.direcciones.[]', function () {
        return this.get('model.direcciones.lastObject');
    }),
    
	actions:{
        toogleError(attr) {
            switch (attr) {
                case 'rfc':
                    this.set('rfcErrorCheck', true)
                    break
                case 'nomRazon':
                    this.set('nomRazonErrorCheck', true)
                    break
                case 'email':
                    this.set('emailErrorCheck', true)
                    break
                case 'cp':
                    this.set('cpErrorCheck', true)
                    break
            }
        },
		remove(){
            var receptor = this.get('model')
            swal({
                title: '¿Esta seguro de eliminar este receptor?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then((result) => {
                if (result.value) {
                    receptor.destroyRecord()
                    this.transitionToRoute('logged.menu.listaclientes')
                }
            })
		},

		save() {
			this.set('isSaving', true);

			let receptor = this.get('model');

			let saveArray = (arr) => {
				return all(arr.invoke('save'));
			};

			return all([
				receptor.get('direcciones').then(saveArray),
				receptor.get('ine').then((ine)=> {if(ine) return ine.fullSave()})
			]).then(() => {
				return receptor.save().then(() => {});
			}).finally(()=>{
				this.set('isSaving', false);
			});
        },
        
        codigoPostal(codigo) {
            return fetch(`http://192.168.100.45:8989/api/cp/${codigo}`).then((response) => {
                response.json().then((datos) => {
                    this.set('codigosPostales', datos.data)
                    this.set('estado', this.codigosPostales.firstObject.estado)
                    this.set('municipio', this.codigosPostales.firstObject.municipio)
                })
            })
        },
		toggleDatosIne(checked){
			if(checked){
				this.set('model.ine', this.get('model.store').createRecord('complemento/ine'))
			}else if(this.get('model.ine.content') && (this.get('model.ine.isNew') || confirm('Va a borrar los datos INE de este cliente ¿desea continuar?'))){
				return this.get('model.ine').then((ine)=> ine.destroyRecord()).then(()=> this.get('model').save());
			}
		}
	}
});
