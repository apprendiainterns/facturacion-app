import Controller from '@ember/controller';
import { inject as service} from '@ember/service';
import {computed} from '@ember/object'
    
export default Controller.extend({
        
    menu: service('lowermenu'),

    daysInMonth: computed('year', 'month', function(){
        let days = moment(`${this.get('year')}-${this.get('month')}`).daysInMonth()
        return Array.apply(null, {length: days}).map(function(a, i){ return `${i+1}`  })
    }),
     
    dateBuilt: computed('year', 'month', 'day', function(){
        let obj = this.getProperties('year', 'month', 'day');
        obj.month -= 1
        return this.set('model.fecha', moment(obj).toDate());
    }),

    searchfactura:computed('model.[]', 'filtro',function(){
        
        if (this.get('filtro') == 'sf' || this.get('filtro') == undefined) {
            return this.get('model')
        }
        if (this.get('filtro') == 'ep' ) {
            return this.get('model').filter((factura)=>{
                return factura.get('prueba') == true
            })
        }
        if (this.get('filtro') == 'c' ) {
            return this.get('model').filter((factura)=>{
                return factura.get('isCancelada') == true
            })
        }

    }),
    /* TO DO
        filtrar factura por fecha
    */
    searchfacturafecha:computed('searchfactura','dateBuilt', function(){
        if (this.get('dateBuilt') == 'Invalid Date') {
            return this.get('searchfactura')
        }
        return this.get('searchfactura').filter((factura)=>{
            return factura.get('fechaUnix') == moment.utc(this.get('dateBuilt')).unix()
        })
    }),

    actions: {
/* 
    Hace invisible el menu inferior para que no tenga otra opcion cuando este facturando
*/
        crearFactura() {
            this.get('menu').invisible()

            let factura = this.store.createRecord('factura')
            this.transitionToRoute('logged.nomenu', factura.id)
        }
    }
});
