import Controller from '@ember/controller';
import ImpuestosGlobalesHandler from 'facturacion-app/mixins/facturas/handlers/impuestos-globales';

export default Controller.extend(ImpuestosGlobalesHandler, {
    actions: {
        backImpuestos() {
            this.transitionToRoute('logged.nomenu.conceptos')
        },
        nextImpuestos() {
            this.set('model.ine', this.get('model.store').createRecord('complemento/ine'))
            this.transitionToRoute('logged.nomenu.complementosIne')
        }
    }
});
