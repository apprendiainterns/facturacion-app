import Controller from '@ember/controller';

export default Controller.extend({
  actions: {
    backResumen() {
      this.transitionToRoute('logged.nomenu.complementosIne')
    },

/* TO DO
    Guardar la factura
 */
    save(options) {
      if (this.get('isSaving')) { return }

      this.set("isSaving", true);

      options = options || {};

      let factura = this.get("model");

      factura.setProperties({
        prueba: !!get(options, 'testOnly'),
        fecha: moment().format()
      });

      return factura.save().then(() => {
        return all([
          all(factura.get("conceptos").invoke('fullSave')),
          all(factura.get("impuestosGlobales").invoke('save')),
          factura.get('ine').then((ine) => { if (ine) return ine.fullSave() })
        ]).then(() => {
          return factura.timbrar().then(() => {
            if (factura.get('serie')) {
              let serie = this.get('serie');
              if (serie) {
                let lastFolio = Math.max(serie.get('lastFolio'), factura.get('folio'));
                serie.set('lastFolio', lastFolio);
              } else {
                serie = this.store.createRecord('facturaSerie', {
                  serie: factura.get('serie'),
                  lastFolio: factura.get('folio')
                });
              }
              serie.save();
            }

            if (get(options, 'sendEmail')) {
              // Send email
              let defaultEmail = factura.get('receptor.email');
              let email = prompt("Escribe la dirección de correo electrónico a la cuál se enviará la factura", defaultEmail);
              return this.transitionToRoute("facturacion.editar", factura);
            }

            return this.transitionToRoute("facturacion.editar", factura);
          }).catch(() => {
            if (this.get('factura.timbrarRequest.error')) {
              alert(this.get('factura.timbrarRequest.error'));
            }
            return this.transitionToRoute("facturacion.editar", factura);
          })
        });
      }).finally(() => {
        this.set("isSaving", false);
      });
    }
  }
});
