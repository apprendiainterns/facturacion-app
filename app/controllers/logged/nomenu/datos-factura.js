import Controller from '@ember/controller';
import { get } from '@ember/object'


export default Controller.extend({

    dayFactura: [],
    monedas: [
        { moneda: 'MXN', tipo: 'Peso Mexiano' },
        { moneda: 'CLP', tipo: 'Peso Chileno' },
        { moneda: 'USD', tipo: 'Dolar Americano' },
        { moneda: 'CAD', tipo: 'Dolar Canadiense' },
        { moneda: 'JPY', tipo: 'Yen' },
        { moneda: 'EUR', tipo: 'Euro' }],

    actions: {
        setMoneda(obj) {
            if (obj) {
                this.set('model.moneda', get(obj, 'moneda'));
                this.set('model.monedaSelected', obj);
            } else {
                this.set('model.moneda', null);
                this.set('model.monedaSelected', null);
            }
        },

        backDatos() {

            this.transitionToRoute('logged.nomenu.emisorReceptor')
        },
        nextDatos() {

            this.transitionToRoute('logged.nomenu.conceptos')
        }
    },

});

