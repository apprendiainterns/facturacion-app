var position = $(window).scrollTop();

$(window).scroll(function () {

    let position2 = $(window).scrollTop();

    var respuesta = false;

    if (position2 >= (position + 140) || position2 <= (position - 140)) {

        if ($('#barraVertical').hasClass('ocultardiv')) {
            $('#barraVertical').toggleClass('ocultardiv')
            respuesta = true
        }

        if (respuesta) {
            setTimeout(function () {
                $('#barraVertical').toggleClass('ocultardiv')
            }, 1500);
        }

        position = position2
    }

})