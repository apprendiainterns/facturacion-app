import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | logged/nomenu/impuestosGlobales', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:logged/nomenu/impuestos-globales');
    assert.ok(route);
  });
});
