import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Controller | logged/detallefactura', function(hooks) {
  setupTest(hooks);

  // Replace this with your real tests.
  test('it exists', function(assert) {
    let controller = this.owner.lookup('controller:logged/detallefactura');
    assert.ok(controller);
  });
});
