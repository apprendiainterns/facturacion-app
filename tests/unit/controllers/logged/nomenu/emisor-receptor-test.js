import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Controller | logged/nomenu/emisor-receptor', function(hooks) {
  setupTest(hooks);

  // Replace this with your real tests.
  test('it exists', function(assert) {
    let controller = this.owner.lookup('controller:logged/nomenu/emisor-receptor');
    assert.ok(controller);
  });
});
